import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamingActionNewsComponent } from './streaming-action-news.component';

describe('StreamingActionNewsComponent', () => {
  let component: StreamingActionNewsComponent;
  let fixture: ComponentFixture<StreamingActionNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StreamingActionNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamingActionNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
