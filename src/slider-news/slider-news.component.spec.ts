import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderNewsComponent } from './slider-news.component';

describe('SliderNewsComponent', () => {
  let component: SliderNewsComponent;
  let fixture: ComponentFixture<SliderNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
