import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleComponent } from './src/sample.component';
import { SliderNewsComponent } from './src/slider-news/slider-news.component';
import { StreamingActionNewsComponent } from './src/streaming-action-news/streaming-action-news.component';
import { SwiperModule } from 'angular2-swiper-wrapper';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';

export * from './src/sample.component';
export * from './src/slider-news/slider-news.component';
export * from './src/streaming-action-news/streaming-action-news.component';

@NgModule({
  imports: [
    CommonModule,
    SwiperModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  declarations: [
    SampleComponent,
    SliderNewsComponent,
    StreamingActionNewsComponent
  ],
  exports: [
    SampleComponent,
    SliderNewsComponent,
    StreamingActionNewsComponent
  ]
})
export class SampleModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SampleModule,
      providers: []
    };
  }
}
